namespace Crib.Test
{
    using Crib;
    using NUnit.Framework;
    using System.Collections.Generic;

    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CanCreateDeck()
        {
            var deck = new Deck();
            Assert.AreEqual(52, deck.Cards.Count);
        }

        [Test]
        public void CanCreateHand()
        {
            var deck = new Deck();
            var hand = new Hand(deck);
            Assert.True(hand.Cards.Count == 4);
            Assert.True(deck.Cards.Count == 48);
        }

        [Test]
        public void CanGetPairs()
        {
            /* One Pair */
            var hand = new Hand(new HashSet<Card>
            {
                new Card(Face.Two, Suit.Clubs),
                new Card(Face.Two, Suit.Diamonds),
                new Card(Face.Three, Suit.Clubs),
                new Card(Face.Four, Suit.Spades)
            });
            var turnCard = new Card(Face.Five, Suit.Hearts);
            var pairs = hand.GetPairs(turnCard);
            Assert.AreEqual(1, pairs);

            /* Two Pairs */
            hand = new Hand(new HashSet<Card> 
            {
                new Card(Face.Two, Suit.Clubs),
                new Card(Face.Two, Suit.Diamonds),
                new Card(Face.Three, Suit.Clubs),
                new Card(Face.Three, Suit.Spades),
            });
            pairs = hand.GetPairs(turnCard);
            Assert.AreEqual(2, pairs);
            
            /* Three Pairs */
            hand = new Hand(new HashSet<Card> 
            {
                new Card(Face.Two, Suit.Clubs),
                new Card(Face.Two, Suit.Diamonds),
                new Card(Face.Two, Suit.Spades),
                new Card(Face.Three, Suit.Spades),
            });
            pairs = hand.GetPairs(turnCard);
            Assert.AreEqual(3, pairs);
            
            /* Four Pairs */
            hand = new Hand(new HashSet<Card> 
            {
                new Card(Face.Two, Suit.Clubs),
                new Card(Face.Two, Suit.Diamonds),
                new Card(Face.Two, Suit.Clubs),
                new Card(Face.Five, Suit.Spades),
            });
            pairs = hand.GetPairs(turnCard);
            Assert.AreEqual(4, pairs);
            
            /* Six Pairs */
            hand = new Hand(new HashSet<Card> 
            {
                new Card(Face.Two, Suit.Clubs),
                new Card(Face.Two, Suit.Diamonds),
                new Card(Face.Two, Suit.Clubs),
                new Card(Face.Two, Suit.Spades),
            });
            pairs = hand.GetPairs(turnCard);
            Assert.AreEqual(6, pairs);
        }

        [Test]
        public void CanGetNibs()
        {
            var hand = new Hand(new HashSet<Card> 
            {
                new Card(Face.Two, Suit.Clubs),
                new Card(Face.Jack, Suit.Diamonds),
                new Card(Face.Eight, Suit.Hearts),
                new Card(Face.Three, Suit.Spades)
            });
            var turnCard = new Card(Face.Six, Suit.Diamonds);
            Assert.IsTrue(hand.GetNibs(turnCard));
            turnCard.Suit = Suit.Hearts;
            Assert.IsFalse(hand.GetNibs(turnCard));
            
        }

        [Test]
        public void CanGetFlush()
        {
            var hand = new Hand(new HashSet<Card> 
            {
                new Card(Face.Two, Suit.Clubs),
                new Card(Face.Jack, Suit.Diamonds),
                new Card(Face.Eight, Suit.Hearts),
                new Card(Face.Three, Suit.Spades)
            });
            var turnCard = new Card(Face.Six, Suit.Diamonds);
            Assert.AreEqual(hand.GetFlush(turnCard), 0);
            Assert.AreEqual(hand.GetFlush(turnCard, true), 0);
            
            hand = new Hand(new HashSet<Card> 
            {
                new Card(Face.Two, Suit.Clubs),
                new Card(Face.Jack, Suit.Clubs),
                new Card(Face.Eight, Suit.Clubs),
                new Card(Face.Three, Suit.Clubs)
            });
            turnCard = new Card(Face.Six, Suit.Diamonds);
            Assert.AreEqual(hand.GetFlush(turnCard), 4);
            Assert.AreEqual(hand.GetFlush(turnCard, true), 0);

            turnCard.Suit = Suit.Clubs;
            Assert.AreEqual(hand.GetFlush(turnCard), 5);
            Assert.AreEqual(hand.GetFlush(turnCard, true), 5);
        }

        [Test]
        public void CanGetFifteens()
        {
            /* Simple case: no fifteens */
            var hand = new Hand(new HashSet<Card> 
            {
                new Card(Face.Ten, Suit.Clubs),
                new Card(Face.Jack, Suit.Diamonds),
                new Card(Face.Queen, Suit.Hearts),
                new Card(Face.King, Suit.Spades)
            });
            var turnCard = new Card(Face.Seven, Suit.Diamonds);
            Assert.AreEqual(0, hand.GetFifteens(turnCard));
            
            /* Simple case: two-card fifteens */
            hand = new Hand(new HashSet<Card> 
            {
                new Card(Face.Ten, Suit.Clubs),
                new Card(Face.Jack, Suit.Diamonds),
                new Card(Face.Queen, Suit.Hearts),
                new Card(Face.King, Suit.Spades)
            });
            turnCard.Face = Face.Five;
            Assert.AreEqual(4, hand.GetFifteens(turnCard));
            
            /* Complex case: multi-card fifteens */
            hand = new Hand(new HashSet<Card> 
            {
                new Card(Face.Six, Suit.Clubs),
                new Card(Face.Five, Suit.Diamonds),
                new Card(Face.Four, Suit.Hearts),
                new Card(Face.Ace, Suit.Spades)
            });
            turnCard.Face = Face.Two;
            Assert.AreEqual(1, hand.GetFifteens(turnCard));
            
            /* Complex case: many multi-card fifteens */
            hand = new Hand(new HashSet<Card> 
            {
                new Card(Face.Six, Suit.Clubs),
                new Card(Face.Five, Suit.Diamonds),
                new Card(Face.Five, Suit.Spades),
                new Card(Face.Four, Suit.Hearts)
            });
            turnCard.Face = Face.Four;
            Assert.AreEqual(4, hand.GetFifteens(turnCard));
            
            /* Complex case: one fifteen requiring all cards */
            hand = new Hand(new HashSet<Card> 
            {
                new Card(Face.Ace, Suit.Clubs),
                new Card(Face.Two, Suit.Diamonds),
                new Card(Face.Three, Suit.Hearts),
                new Card(Face.Three, Suit.Spades)
            });
            turnCard.Face = Face.Six;
            Assert.AreEqual(1, hand.GetFifteens(turnCard));
        }
    }
}