﻿namespace Crib
{
    using System;
    public class Card
    {
        public Face Face { get; set; }
        public Suit Suit { get; set; }

        public Card(Face face, Suit suit)
        {
            Face = face;
            Suit = suit;
        }

        public int Value
        {
            get
            {
                if (Face == Face.Jack || Face == Face.Queen || Face == Face.King)
                {
                    return 10;
                }
                else if (Face == Face.Ace)
                {
                    return 1;
                }
                return Convert.ToInt32(Face);
            }
        }
    }
}
