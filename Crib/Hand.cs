﻿using System;
using System.Linq;

namespace Crib
{
    using System.Collections.Generic;
    using static Utils;

    public class Hand : ICardContainer
    {
        public HashSet<Card> Cards { get; }

        private IScore Score { get; set; }

        public Hand(Deck deck)
        {
            var random = new Random();
            var cards = new HashSet<Card>();
            for (var i = 0; i < 4; i++)
            {
                var index = random.Next(deck.Cards.Count);
                cards.Add(deck.Cards.ElementAt(index));
                deck.Cards.Remove(cards.ElementAt(i));
            }

            Cards = cards;
        }

        public Hand(HashSet<Card> cards)
        {
            Cards = cards;
        }

        private class IScore
        {
            public IScore(Hand hand, Card turnCard)
            {
                Pairs = hand.GetPairs(turnCard);
                Runs = hand.GetRuns(turnCard);
                Nibs = hand.GetNibs(turnCard);
                Flush = hand.GetFlush(turnCard);
                Fifteens = hand.GetFifteens(turnCard);
            }

            private int Pairs { get; set; }

            private Dictionary<string, int> Runs { get; set; }

            private bool Nibs { get; set; }

            private int Flush { get; set; }

            public int Fifteens { get; set; }

            public int Value()
            {
                return 2 * Pairs + 2 * Fifteens + (Nibs ? 1 : 0) + Flush;
            }
        }

        public int GetScore(Card turnCard)
        {
            Score = new IScore(this, turnCard);
            return Score.Value();
        }

        public int GetPairs(Card turnCard)
        {
            var allCards = new List<Card>(Cards);
            allCards.Add(turnCard);
            return CompareCards(allCards, (cards) => cards.All(card => card.Face.Equals(cards[0].Face)), true);
        }

        public Dictionary<string, int> GetRuns(Card turnCard)
        {
            return new Dictionary<string, int>();
        }

        public bool GetNibs(Card turnCard)
        {
            var jacks = Cards.Where(card => card.Face == Face.Jack).ToList();
            if (jacks.Count == 0)
            {
                return false;
            }

            return jacks.Any(jack => jack.Suit == turnCard.Suit);
        }

        public int GetFlush(Card turnCard, bool isCrib = false)
        {
            if (Cards.All(card => card.Suit == Cards.ElementAt(0).Suit))
            {
                if (turnCard.Suit == Cards.ElementAt(0).Suit)
                {
                    return 5;
                }

                return isCrib ? 0 : 4;
            }

            return 0;
        }

        public int GetFifteens(Card turnCard)
        {
            var allCards = new List<Card>(Cards);
            allCards.Add(turnCard);
            return CompareCards(allCards, (cards) => cards.Sum(card => card.Value) == 15);
        }

        private static int CompareCards(IList<Card> cards, Func<IList<Card>, bool> predicate, bool comparePairs = false)
        {
            var count = 0;
            foreach (var index in Indices(comparePairs))
            {
                var tempCards = new List<Card>();
                foreach (var i in index)
                {
                    tempCards.Add(cards[i]);
                }

                if (predicate(tempCards))
                {
                    count++;
                }
            }

            return count;
        }
    }
}