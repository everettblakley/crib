﻿using System.Linq;

namespace Crib
{
    using System;
    class Program
    {
        private Deck Deck { get; set; }
        private Hand Hand { get; set; }
        
        private Card Turn { get; set; }
        
        public Program()
        {
            Deck = new Deck();
            Hand = new Hand(Deck);
            
            var random = new Random();
            var turnIndex = random.Next(Deck.Cards.Count);
            Turn = Deck.Cards.ElementAt(turnIndex);
            Deck.Cards.Remove(Turn);
        }
        static void Main(string[] args)
        {
            var  p = new Program();
            Console.Write("Player Hand: ");
            foreach (var card in p.Hand.Cards)
            {
                Console.Write(card.Face + " of " + card.Suit + ", ");
            }
            Console.Write("\n");
            Console.WriteLine("Turn Card: " + p.Turn.Face + " of " + p.Turn.Suit);
            Console.WriteLine("Score is: " + p.Hand.GetScore(p.Turn));
        }
    }
}
