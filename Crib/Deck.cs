﻿namespace Crib
{
    using System.Collections.Generic;
    
    public class Deck : ICardContainer
    {
        public HashSet<Card> Cards { get; set; }

        public Deck()
        {
            Cards = new HashSet<Card>();
            foreach (var face in Utils.GetValues<Face>())
            {
                foreach (var suit in Utils.GetValues<Suit>())
                {
                    Cards.Add(new Card(face, suit));
                }
            }
        }
    }
}