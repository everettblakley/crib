﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Crib
{
    public class Utils
    {
        public static int Choose(int n, int k)
        {
            if (k > n)
            {
                throw new Exception("Invalid combination");
            }

            return Factorial(n) / (Factorial(k) * Factorial(n - k));
        }

        public static int Factorial(int n)
        {
            if (n < 0)
            {
                throw new Exception("Invalid factorial");
            }
            if (n == 0)
            {
                return 1;
            }
            else return n * Factorial(n - 1);
        }

        public static IEnumerable<int[]> Indices(bool pairs)
        {
            if (pairs.Equals(true))
            {
                return new int[][]
                {
                    new int[] {0, 1}, new int[] {0, 2}, new int[] {0, 3}, new int[] {0, 4},
                    new int[] {1, 2}, new int[] {1, 3}, new int[] {1, 4},
                    new int[] {2, 3}, new int[] {2, 4},
                    new int[] {3, 4}
                };
            }
            return new int[][]
            {
                // Single comparison indices
                new int[] {0, 1}, new int[] {0, 2}, new int[] {0, 3}, new int[] {0, 4},
                new int[] {1, 2}, new int[] {1, 3}, new int[] {1, 4},
                new int[] {2, 3}, new int[] {2, 4},
                new int[] {3, 4},
                // Triples
                new int[] {0, 1, 2}, new int[] {0, 1, 3}, new int[] {0, 1, 4}, new int[] {0, 2, 3}, new int[] {0, 2, 4}, new int[] {0, 3, 4},
                new int[] {1, 2, 3}, new int[] {1, 2, 4}, new int[] {1, 3, 4},
                new int[] {2, 3, 4},
                // Quads
                new int[] {0, 1, 2, 3}, new int[] {0, 1, 2, 4}, new int[] {0, 1, 3, 4}, new int[] {1, 2, 3, 4},
                // All
                new int[] {0, 1, 2, 3, 4}
            };
        }
        
        public static IEnumerable<T> GetValues<T>() {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
    }
}