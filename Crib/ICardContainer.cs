﻿namespace Crib
{
    using System.Collections.Generic;

    interface ICardContainer
    {
        HashSet<Card> Cards { get; }
    }
}
